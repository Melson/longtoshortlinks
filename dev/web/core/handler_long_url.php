<?php

class Handler_long_url {

    private $long_url;
    private $short_url;

    const HOST = 'db'; // адрес сервера 
    const USER = 'root'; // имя пользователя
    const PASSWORD = '1234'; // пароль
    const DATABASE = 'short_url'; // имя базы данных
    const DATABASETABLE = 'short_url_table'; // имя таблицы

    function start_handler( $url = '' ){

        $response = '';

        if($url){

            $existence_url = $this->check_url($url);

            if( $existence_url == true ){
                preg_match('/^https?:\/\/([^\/]+)/', $this->long_url, $host_item);
                $response = array(
                    'url'   => $this->long_url,
                    'text'  => $host_item[0].'/'.$this->short_url,
                );
            } else{
                $long_short_url = $this->create_short_url($url);
                preg_match('/^https?:\/\/([^\/]+)/', $this->long_url, $host_item);
                $response = array(
                    'url'   => $this->long_url, 
                    'text'  => $host_item[0].'/'.$this->short_url,
                );
            }
        }

        return $response;
    }

    function check_url( $url = '' ){

        $link = $this->connectDB();
        $table = $this::DATABASETABLE;

        $sql_check_url = "SELECT * FROM $table WHERE long_url='$url' ";
        $result = $link->query($sql_check_url);

        if( $result->num_rows > 0 ){

            $row_res = $result->fetch_assoc();
            $this->long_url = $row_res['long_url'];
            $this->short_url = $row_res['code'];

            return true;
        } else{
            return false;
        }
    }

    function connectDB(){

        $link = mysqli_connect($this::HOST, $this::USER, $this::PASSWORD, $this::DATABASE);
        if (!$link) {
            die('Ошибка соединения: ' . mysql_error());
        } else{
            return $link; 
        }
    }

    function create_short_url( $url = '' ){

        $string = "qwertyuiopasdfghjklzxcvbnm1234567890";
        
        $short_url = parse_url($url, PHP_URL_PATH);
        preg_match('/^https?:\/\/([^\/]+)/', $url, $urlItem);
        $host = $urlItem[0];
        
        while( strlen($short_url) > 5 ){
            
            $item = '';
            for ($i=0; $i < strlen($short_url)-1; $i++) { 
                
                $item .= $string[fmod( ord($short_url[$i]), strlen($string) )];
            }
            $short_url = $item;
        }

        $link = $this->connectDB();
        $table = $this::DATABASETABLE;

        $sql_insert_new_url = "INSERT INTO $table VALUES(NULL, '$url', '$short_url')";
        $result = mysqli_query($link, $sql_insert_new_url) or die("Ошибка " . mysqli_error($link));

        $this->long_url = $url;
        $this->short_url = $short_url;

        if( $result ){
            return true;
        } else{
            return false;
        }
    }

}