<?php

if(isset( $_POST['long_url'] )) {
    
    require_once 'handler_long_url.php';
    $handler_long_url = new Handler_long_url();
    $result = $handler_long_url->start_handler($_POST['long_url']);
    
    echo json_encode($result);
}