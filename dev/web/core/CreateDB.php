<?php

class CreateDB {

    const HOST = 'db'; // адрес сервера 
    const USER = 'root'; // имя пользователя
    const PASSWORD = '1234'; // пароль
    const DATABASE = 'short_url'; // имя базы данных
    const DATABASETABLE = 'short_url_table'; // имя таблицы

    function __construct(){

        $DATABASE = $this::DATABASE;
        $DATABASETABLE = $this::DATABASETABLE;

        $link = mysqli_connect($this::HOST, $this::USER, $this::PASSWORD);
        if (!$link) {
            die('Ошибка соединения: ' . mysql_error());
        }

        $sql_create_db = "CREATE DATABASE `$DATABASE`";
        if ($link->query($sql_create_db)) {
            echo "База ".$DATABASE." успешно создана\n";
        } else {
            // echo 'Ошибка при создании базы данных: ' . $link->error . "\n";
        }

        $db_connect = mysqli_connect($this::HOST, $this::USER, $this::PASSWORD, $DATABASE); 
        $sql_create_table = "CREATE TABLE `$DATABASETABLE`(
                                                id INT(6) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                                long_url VARCHAR(255) NOT NULL,
                                                code VARCHAR(5) NOT NULL
                                            )";
        if ($db_connect->query($sql_create_table)) {
            echo "Таблица ".$DATABASETABLE." успешно создана\n";
        } else {
            // echo 'Ошибка при создании таблицы в БД: ' . $db_connect->error . "\n";
        }
    }

}