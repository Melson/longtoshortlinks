<?php

class Setup {

    const CSS_FILES = array(
        '/assets/css/bootstrap4_grid.css',
        '/assets/css/style.css',
    );

    const JS_FILES = array(
        'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',
        '/assets/js/main.js',
        '/assets/js/ajax.js',
    );

    public function header_css(){

        foreach ($this::CSS_FILES as $single_file) {
            echo '<link rel="stylesheet" href="'.$single_file.'">';
        }
    }

    public function footer_js(){

        foreach ($this::JS_FILES as $single_file) {
            echo '<script src="'.$single_file.'"></script>';
        }
    }

}