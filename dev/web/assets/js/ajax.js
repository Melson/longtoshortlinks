$('form').on('submit', function(e){

    e.preventDefault();

    let data = { 
        long_url: $('#long_url').val(),
    };

    $.ajax({
        url: '../../core/Ajax.php',
        type        : 'POST',
        data        : data,
        cache       : false,
        dataType    : 'json',
        success: function(responce){
            if(responce){
                $('#result a').attr('href', responce.url);
                $('#result a').html(responce.text);
            } else{
                $('#result').html('Что то пошло не так!');
            }
        }
    });
});