<?php require_once 'functions.php'; ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Short_URL</title>
    <?php $setup->header_css(); ?>
</head>
<body>
    <section>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="form_wrap">

                        <form class="d-flex flex-column">
                            <h1>Укоротитель ссылок</h1>
                            <input id="long_url" name='long_url' type="text" value="https://music.yandex.ru/artist/762002/albums">
                            <button type="submit">Получить короткую ссылку</button>
                            <div id="result"><a href="" target="_blank"></a></div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <footer>
        <?php $setup->footer_js(); ?>
    </footer>
</body>
</html>